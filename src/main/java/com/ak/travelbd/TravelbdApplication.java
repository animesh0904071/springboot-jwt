package com.ak.travelbd;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TravelbdApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelbdApplication.class, args);
	}

}
