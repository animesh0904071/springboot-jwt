package com.ak.travelbd.util;

public class HeaderMissingException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public HeaderMissingException(String message) {
        super(message);
    }

    public HeaderMissingException(String message, Throwable cause) {
        super(message, cause);
    }
}
