package com.ak.travelbd.controllers;

import javax.validation.Valid;

import com.ak.travelbd.services.AuthService;
import com.ak.travelbd.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ak.travelbd.payload.request.LoginRequest;
import com.ak.travelbd.payload.request.SignupRequest;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthController {

    @Autowired
    AuthService authService;

    @PostMapping("/signin")
    public ResponseEntity<RestResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {

        RestResponse response = new RestResponse();

        try {
            response.setData(authService.signInWithToken(loginRequest));
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(e.getMessage());
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }

    @PostMapping("/signup")
    public ResponseEntity<RestResponse> registerUser(@Valid @RequestBody SignupRequest signUpRequest) {

        RestResponse response = new RestResponse();

        try {
            response.setData(authService.signUp(signUpRequest));
            response.addMessage("User registered successfully!");
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(e.getMessage());
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }
}