package com.ak.travelbd.controllers;

import com.ak.travelbd.util.Messages;
import com.ak.travelbd.util.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {

    @Autowired
    Messages messages;

    @GetMapping("/all")
    public ResponseEntity<RestResponse> allAccess() {

        RestResponse response = new RestResponse();
        try {
            response.addMessage(messages.getMessage("public.url.access"));
            response.setData("Public Content");
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(messages.getMessage("url.access.failed"));
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<RestResponse> userAccess() {
        RestResponse response = new RestResponse();
        try {
            response.addMessage(messages.getMessage("user.url.access"));
            response.setData("User Content");
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(messages.getMessage("url.access.failed"));
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }

    @GetMapping("/mod")
    @PreAuthorize("hasRole('MODERATOR')")
    public ResponseEntity<RestResponse> moderatorAccess() {
        RestResponse response = new RestResponse();
        try {
            response.addMessage(messages.getMessage("mod.url.access"));
            response.setData("Moderator Content");
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(messages.getMessage("url.access.failed"));
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<RestResponse> adminAccess() {
        RestResponse response = new RestResponse();
        try {
            response.addMessage(messages.getMessage("admin.url.access"));
            response.setData("Admin Content");
        } catch (Exception e) {
            response.setError(true);
            response.addMessage(messages.getMessage("url.access.failed"));
        }

        return new ResponseEntity<RestResponse>(response, HttpStatus.OK);
    }
}