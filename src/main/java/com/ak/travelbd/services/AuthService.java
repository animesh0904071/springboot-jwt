package com.ak.travelbd.services;

import com.ak.travelbd.models.User;
import com.ak.travelbd.payload.request.LoginRequest;
import com.ak.travelbd.payload.request.SignupRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface AuthService {
    ResponseEntity<?> signInWithToken(LoginRequest username) throws UsernameNotFoundException;

    User signUp(SignupRequest signUpRequest) throws Exception;
}
